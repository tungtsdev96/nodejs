const { Router } = require('express');
const { inject } = require('awilix-express');


const HomeController = {
    get router() {
      const router = Router();

      router.get('/home', inject('userRepository'), this.showForm);

      return router;
    },
    
    async showForm(req, res){
        let {userRepository} = req;
        let user = {
          id : req.body.id,
          name : 'Pham Hai Linh',
          email : 'linhphamdev96@gmail.com',
          phone_number : '0367645238',
          dob : '1996-11-30'
        }
        var newUser = await userRepository.createUser(user);
        if(newUser){
           res.json({
             statuss : 'OK',
             data : newUser
           });
        }else{
           res.json({
              status : 500
           });
        }
    }
  };
  
  module.exports = HomeController;