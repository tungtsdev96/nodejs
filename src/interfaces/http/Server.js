const express = require('express');
const path = require('path');

class Server {
    constructor({
        router
    }){
        this.express = express();
        this.router = router;
        this.express.set('views', path.join(__dirname, 'views'));
        this.express.set('view engine', 'ejs');
        this.express.use(express.static('uploads'));
        this.express.use("/public", express.static(path.join(__dirname, 'public')));
        this.express.disable('x-powered-by');
        this.express.use(router);
    }

    start(){
        return new Promise((resolve) => {
            const http = this.express.listen(3000, () => {
                resolve();
            })
        });
        
    }
}
module.exports = Server;

