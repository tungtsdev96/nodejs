const {
    Router
} = require('express');
const {
    inject
} = require('awilix-express');

const bodyParser = require('body-parser');
const controller = require('./controller/utils/createControllerRoutes');
var jwt = require('jsonwebtoken');
const cors = require('cors');
const methodOverride = require('method-override');
module.exports = ({
    containerMiddleware,
    swaggerMiddleware
}) => {
    const router = Router();
    const apiRouter = Router();
    const webRouter = Router();
    webRouter
        .use(methodOverride('X-HTTP-Method-Override'))
        .use(cors())
        .use(bodyParser.json())
        .use(containerMiddleware)
        .use(bodyParser.urlencoded({
            extended: true
        }));

    apiRouter
        .use(methodOverride('X-HTTP-Method-Override'))
        .use(cors())
        .use(bodyParser.json())
        // .use(compression())
        .use(containerMiddleware)
        // .use('/docs', swaggerMiddleware);


    //For Web
    webRouter.use('/home', controller('api/home/HomeController'));


    //For API
    


    // Import
    router.use('/', webRouter);
    router.use('/api', apiRouter);
    //Swagger
    router.use('/docs', swaggerMiddleware);
    return router;
}