const {
    createContainer,
    asClass,
    asFunction,
    asValue
} = require('awilix');
const {
    scopePerRequest
} = require('awilix-express');

const config = require('../config');

const container = createContainer();

//Import Models
const User = require('./infra/database/models/User');

//Import Repository


const UserRepository = require('./infra/repository/user/UserRepository');

// Import Server
const Server = require('./interfaces/http/Server')
const router = require('./interfaces/http/router');

const Application = require('./app/Application');

// Import middleware

const swaggerMiddleware = require('./interfaces/http/swagger/swaggerMiddleware')

//System
container.register({
    config : asValue(config)
})

//Middleware

container.register({
    containerMiddleware: asValue(scopePerRequest(container)),
    swaggerMiddleware : asValue(swaggerMiddleware),
})
// Repository

container.register({
    userRepository : asClass(UserRepository).singleton()
})


// Models
container.register({
    UserModel : asValue(User)
})

// Server
container.register({
    app : asClass(Application).singleton(),
    server : asClass(Server),
    router : asFunction(router)
})


module.exports = container;
