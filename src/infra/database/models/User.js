const Sequelize = require('sequelize');
const {sequelize, Op} = require('../LoadDatabase');

const User = sequelize.define('user', {
    id : {
        type : Sequelize.INTEGER,
        primaryKey : true
    },
    name : {
        type : Sequelize.STRING
    },
    email : {
        type : Sequelize.TINYINT
    },
    phone_number : {
        type : Sequelize.STRING
    },
    dob : {
        type : Sequelize.DATE
    }
},{
        timestamps : false
    }
);
module.exports = User;