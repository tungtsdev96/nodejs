const Sequelize = require('sequelize');
const { db: config } = require('config');
console.log(config);
if(config) {
  const sequelize = new Sequelize(config);
  const Op = Sequelize.Op;
  module.exports = {
      sequelize, Op
  }
} else {
  console.error('Database configuration not found, disabling database.');
}

