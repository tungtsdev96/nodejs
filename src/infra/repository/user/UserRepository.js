

class UserRepository {

    constructor({
        UserModel
    }){
        this.userModel = UserModel;
    }

    async getUser(id){
        
    }

    async createUser(user){
        try {
            let newUser = await this.userModel.create({
                ...user
            });
            return newUser;
        } catch (error) {
            console.log(error);
            return null;
        }
        
    }
}

module.exports = UserRepository;